package br.com.githubapi.model;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.githubapi.util.Validator;

/**
 * Model class of owner of repository object with basic params about user
 * Base API: https://api.github.com/search/repositories
 * Object: owner
 */
public class OwnerRepository {
    // Params of the API response JSON
    protected static final String TAG_LOGIN = "login";
    protected static final String TAG_AVATAR = "avatar_url";
    protected static final String TAG_URL = "url";

    /** Username **/
    protected String login;
    /** URL of the profile image **/
    protected String avatarUrl;
    /** URL to get user attributes from API **/
    protected String url;

    public OwnerRepository(JSONObject owner) throws JSONException {
        this.login = owner.has(TAG_LOGIN) ? Validator.validStr(owner.getString(TAG_LOGIN)) : "";
        this.avatarUrl = owner.has(TAG_AVATAR) ? Validator.validStr(owner.getString(TAG_AVATAR)) : "";
        this.url = owner.has(TAG_URL) ? Validator.validStr(owner.getString(TAG_URL)) : "";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
