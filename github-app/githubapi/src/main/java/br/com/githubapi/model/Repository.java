package br.com.githubapi.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Model class of repository object
 * Base API: https://api.github.com/search/repositories
 */
public class Repository {
    // Params of the API response JSON
    private static final String TAG_NAME = "name";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_STARS = "stargazers_count";
    private static final String TAG_FORKS = "forks_count";
    private static final String TAG_OWNER = "owner";

    private String name;
    private String description;
    private int stars;
    private int forks;
    public User user;

    public Repository(JSONObject repository) throws JSONException {
        this.name = repository.has(TAG_NAME) ? repository.getString(TAG_NAME) : "";
        this.description = repository.has(TAG_DESCRIPTION) ? repository.getString(TAG_DESCRIPTION) : "";
        this.stars = repository.has(TAG_STARS) ? repository.getInt(TAG_STARS) : 0;
        this.forks = repository.has(TAG_FORKS) ? repository.getInt(TAG_FORKS) : 0;

        if(repository.has(TAG_OWNER)) {
            this.user = new User(repository.getJSONObject(TAG_OWNER));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public void setUser(User user){
        this.user = user;
    }
}