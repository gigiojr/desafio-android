package br.com.githubapi.model;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.githubapi.util.Validator;

/**
 * Model class to user object
 * Base API: https://api.github.com/users/<username>
 */
public class User extends OwnerRepository{
    // Params of the API response JSON
    private static final String TAG_NAME = "name";

    private String name;

    public User(JSONObject obj) throws JSONException {
        super(obj);

        this.name = obj.has(TAG_NAME) ? Validator.validStr(obj.getString(TAG_NAME)) : "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
