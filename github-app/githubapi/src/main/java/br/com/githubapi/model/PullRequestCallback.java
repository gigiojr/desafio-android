package br.com.githubapi.model;

import java.util.List;

/**
 * Callback interface to requests pull requests from API
 * Base API: https://api.github.com/repos/<login>/<repositório>/pulls
 */
public interface PullRequestCallback {
    /**
     * Called when request finished without errors
     * @param list Pull request list get from API
     */
    void onSuccess(List<PullRequest> list);

    /**
     * Called when request have an error
     */
    void onError();
}
