package br.com.githubapi.model;

import java.util.List;

/**
 * Callback interface to requests repositories from API
 * Base API: https://api.github.com/search/repositories
 */
public interface RepositoryCallback {
    /**
     * Called when request finished without errors
     * @param list Repository list get from API
     */
    void onSuccess(List<Repository> list);

    /**
     * Called when request have an error
     */
    void onError();
}
