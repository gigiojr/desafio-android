package br.com.githubapi.model;

/**
 * Callback interface to requests to API
 * Base API: https://api.github.com/users/<login>
 */
public interface UserCallback {
    /**
     * Called when request finished without errors
     * @param user User object get from API
     */
    void onSuccess(User user);

    /**
     * Called when request have an error
     */
    void onError();
}