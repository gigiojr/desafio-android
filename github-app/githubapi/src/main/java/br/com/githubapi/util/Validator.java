package br.com.githubapi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class with validations of the response API elements
 */
public class Validator {
    /**
     * Valid an String.
     * @param str
     * @return The same string, if valid, or blank String, if not.
     */
    public static String validStr(String str){
        return str == null || str.isEmpty() || str.equals("null") ? "" : str;
    }

    /**
     * Valid an Date from String.
     * @param str Date in String format.
     * @return The date from String.
     */
    public static Date validDate(String str){
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
