package br.com.githubapi;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.githubapi.model.PullRequest;
import br.com.githubapi.model.PullRequestCallback;
import br.com.githubapi.model.Repository;
import br.com.githubapi.model.RepositoryCallback;
import br.com.githubapi.model.User;
import br.com.githubapi.model.UserCallback;

/**
 * Class to request Github API
 */
public class Github {
    // Params of the API response JSON
    private static final String TAG_ITEMS = "items";

    //Base URLs to request API
    private static final String URL_REPOSITORIES = "https://api.github.com/search/repositories";
    private static final String URL_REPOSITORY = "https://api.github.com/repos/";

    /**
     * Get one page of JAVA repositories sorted by stars.
     * @param context Application context.
     * @param page Number of page.
     * @param callback Callback to return request result.
     */
    public void getRepositories(final Context context, int page, final RepositoryCallback callback){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = URL_REPOSITORIES + "?q=language:Java&sort=stars&page=" + page;

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null,
            new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if(response.has(TAG_ITEMS)) {
                    try {
                        List<Repository> list = new ArrayList<>();
                        JSONArray array = response.getJSONArray(TAG_ITEMS);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            Repository repository = new Repository(obj);
                            list.add(repository);
                        }
                        callback.onSuccess(list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callback.onError();
                    }
                } else{
                    callback.onError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                error.printStackTrace();
                callback.onError();
            }
        });

        queue.add(jsonRequest);
    }

    /**
     * Get data about an user.
     * @param context Application context.
     * @param url URL to get information.
     * @param callback Callback to return request result.
     */
    public void getUser(Context context, String url, final UserCallback callback){
        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
            new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    User user = new User(response);
                    callback.onSuccess(user);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                error.printStackTrace();
                callback.onError();
            }
        });

        queue.add(jsonObjectRequest);
    }

    /**
     * Get pull requests of an repository.
     * @param context Application context.
     * @param userName User owner of the repository.
     * @param repositoryName Name of the repository.
     * @param callback Callback to return request result.
     */
    public void getPullRequests(Context context, String userName, String repositoryName, final PullRequestCallback callback){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = URL_REPOSITORY + userName + "/" + repositoryName + "/pulls?state=all";

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, null,
            new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                try {
                    List<PullRequest> list = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject obj = response.getJSONObject(i);
                        PullRequest pullRequest = new PullRequest(obj);
                        list.add(pullRequest);
                    }
                    callback.onSuccess(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                error.printStackTrace();
                callback.onError();
            }
        });

        queue.add(jsonRequest);
    }
}
