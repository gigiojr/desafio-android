package br.com.githubapi.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import br.com.githubapi.util.Validator;

/**
 * Model class to pull request object
 * Base API: https://api.github.com/repos/<login>/<repositório>/pulls
 */
public class PullRequest {
    // Params of the API response JSON
    private static final String TAG_TITLE = "title";
    private static final String TAG_BODY = "body";
    private static final String TAG_STATE = "state";
    private static final String TAG_USER = "user";
    private static final String TAG_CREATED = "created_at";
    private static final String TAG_URL = "html_url";

    private String title;
    private String body;
    private String state;
    private String url;
    private Date created;
    public User user;

    public PullRequest(JSONObject repository) throws JSONException {
        this.title = repository.has(TAG_TITLE) ? Validator.validStr(repository.getString(TAG_TITLE)) : "";
        this.body = repository.has(TAG_BODY) ? Validator.validStr(repository.getString(TAG_BODY)) : "";
        this.state = repository.has(TAG_STATE) ? Validator.validStr(repository.getString(TAG_STATE)) : "";
        this.url = repository.has(TAG_URL) ? Validator.validStr(repository.getString(TAG_URL)) : "";
        this.created = repository.has(TAG_CREATED) ? Validator.validDate(repository.getString(TAG_CREATED)) : null;

        if(repository.has(TAG_USER)) {
            this.user = new User(repository.getJSONObject(TAG_USER));
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
