package br.com.concrete.desafio.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import br.com.concrete.desafio.R;
import br.com.concrete.desafio.databinding.ActivityRepositoryListBinding;
import br.com.concrete.desafio.util.RecycleMultiLayout;
import br.com.concrete.desafio.model.card.IconTextCard;
import br.com.concrete.desafio.model.card.ProfileVerticalCard;
import br.com.concrete.desafio.model.card.RepositoryCard;
import br.com.concrete.desafio.model.card.RepositoryCardInterface;
import br.com.concrete.desafio.model.card.TitleTextCard;

import br.com.githubapi.Github;
import br.com.githubapi.model.Repository;
import br.com.githubapi.model.RepositoryCallback;
import br.com.githubapi.model.User;
import br.com.githubapi.model.UserCallback;

/**
 * Activity to list of repositories
 * XML: res/layout/activity_repository_list
 *
 * @see RepositoryCardInterface
 * @see RecycleMultiLayout
 * @see Github
 */
public class RepositoryListActivity extends AppCompatActivity implements RepositoryCardInterface{
    private static final String TAG = "RepositoryListActivity";

    private Github githubAPI;
    private RecycleMultiLayout recycle;

    private int nextPage;
    private Boolean searching;
    private Boolean endPage;

    private int amountInsert;
    private int amountTotal;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityRepositoryListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_repository_list);

        this.recycle = new RecycleMultiLayout();
        binding.setRecycle(this.recycle);

        RecyclerView recyclerView = findViewById(R.id.recycle);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && !endPage &&  !searching) {
                    Log.i(TAG, "END SCROLL");
                    getRepositories();
                }
            }
        });

        this.endPage = false;
        this.nextPage = 1;

        this.githubAPI = new Github();
        this.getRepositories();
    }

    @Override
    public void onRepositoryClick(RepositoryCard card) {
        Intent intent = new Intent(this, PullrequestListActivity.class);
        intent.putExtra("user", card.getProfile().getLogin());
        intent.putExtra("repository", card.getTitle().getTitle());
        startActivity(intent);
    }

    /**
     * Call Github API to get list of repositories.
     */
    private void getRepositories(){
        this.searching = true;
        this.dialog = ProgressDialog.show(this, "", "Buscando...", true, false);
        this.githubAPI.getRepositories(this, this.nextPage, new RepositoryCallback() {
            @Override
            public void onSuccess(List<Repository> list) {
                if(list != null && list.size() > 0) {
                    amountInsert = 0;
                    amountTotal = list.size();
                    for (Repository repository : list) {
                        getUser(repository);
                    }
                    nextPage += 1;
                } else{
                    endPage = true;
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "Error in connection with Github API");
                dialog.dismiss();
                searching = false;

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(RepositoryListActivity.this);
                alertDialog.setTitle("Erro");
                alertDialog.setMessage("Não foi possível buscar os registros.");
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
    }

    /**
     * Call Github API to get params about user owner of the repository.
     * @param repository Repository object.
     */
    private void getUser(final Repository repository){
        githubAPI.getUser(this, repository.user.getUrl(), new UserCallback() {
            @Override
            public void onSuccess(User user) {
                repository.setUser(user);
                includeCard(repository);
            }

            @Override
            public void onError() {
                includeCard(repository);
            }
        });
    }

    /**
     * Include repository on recycler list.
     * @param repository
     */
    private void includeCard(Repository repository){
        TitleTextCard title = new TitleTextCard(repository.getName(), repository.getDescription());
        ProfileVerticalCard profile = new ProfileVerticalCard(repository.user.getLogin(), repository.user.getName(), repository.user.getAvatarUrl());
        IconTextCard fork = new IconTextCard(String.valueOf(repository.getForks()), getResources().getDrawable(R.drawable.ic_fork));
        IconTextCard star = new IconTextCard(String.valueOf(repository.getStars()), getResources().getDrawable(R.drawable.ic_star));

        RepositoryCard repositoryCard = new RepositoryCard(title, profile, fork, star);
        repositoryCard.setEvents(this);
        this.recycle.list.add(repositoryCard);

        this.amountInsert += 1;
        if(this.amountInsert == this.amountTotal){
            this.dialog.dismiss();
            this.searching = false;
        }
    }
}