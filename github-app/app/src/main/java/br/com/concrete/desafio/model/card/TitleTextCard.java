package br.com.concrete.desafio.model.card;

import br.com.concrete.desafio.BR;
import br.com.concrete.desafio.R;
import br.com.concrete.desafio.util.RecycleMultiLayoutInterface;

/**
 * Layout Model Class to show an title and an text.
 *
 * xml: res/layout/card_title_text
 * data: card
 *
 * @see RecycleMultiLayoutInterface
 * @see br.com.concrete.desafio.util.RecycleMultiLayout
 */
public class TitleTextCard implements RecycleMultiLayoutInterface {
    private String title;
    private String text;

    public TitleTextCard(String title, String text) {
        this.title = title;
        this.text = text;
    }

    @Override
    public int getVariable() {
        return BR.card;
    }

    @Override
    public int getLayout() {
        return R.layout.card_title_text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
