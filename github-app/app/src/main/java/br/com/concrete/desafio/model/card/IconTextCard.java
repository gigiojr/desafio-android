package br.com.concrete.desafio.model.card;

import android.graphics.drawable.Drawable;

import br.com.concrete.desafio.BR;
import br.com.concrete.desafio.R;
import br.com.concrete.desafio.util.RecycleMultiLayoutInterface;

/**
 * Layout Model Class to show an icon and an text.
 *
 * xml: res/layout/card_icon_text
 * data: card
 *
 * @see RecycleMultiLayoutInterface
 * @see br.com.concrete.desafio.util.RecycleMultiLayout
 */
public class IconTextCard implements RecycleMultiLayoutInterface{
    private String text;
    private Drawable icon;

    public IconTextCard(String text, Drawable icon) {
        this.text = text;
        this.icon = icon;
    }

    @Override
    public int getVariable() {
        return BR.card;
    }

    @Override
    public int getLayout() {
        return R.layout.card_icon_text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
