package br.com.concrete.desafio.model.card;

import br.com.concrete.desafio.BR;
import br.com.concrete.desafio.R;
import br.com.concrete.desafio.util.RecycleMultiLayoutInterface;

/**
 * Layout Model Class to show photo, username and name on vertical layout.
 *
 * xml: res/layout/card_profile_vertical
 * data: card
 *
 * @see RecycleMultiLayoutInterface
 * @see br.com.concrete.desafio.util.RecycleMultiLayout
 */
public class ProfileVerticalCard extends ProfileCard implements RecycleMultiLayoutInterface {

    public ProfileVerticalCard(String username, String name, String imageUrl) {
        super(username, name, imageUrl);
    }

    @Override
    public int getVariable() {
        return BR.card;
    }

    @Override
    public int getLayout() {
        return R.layout.card_profile_vertical;
    }
}
