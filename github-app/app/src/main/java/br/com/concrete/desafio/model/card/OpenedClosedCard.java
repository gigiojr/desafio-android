package br.com.concrete.desafio.model.card;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import br.com.concrete.desafio.BR;
import br.com.concrete.desafio.R;
import br.com.concrete.desafio.util.RecycleMultiLayoutInterface;

/**
 * Layout Model Class to show pull requests opened and closed.
 *
 * xml: res/layout/card_opened_closed
 * data: card
 *
 * @see RecycleMultiLayoutInterface
 * @see br.com.concrete.desafio.util.RecycleMultiLayout
 */
public class OpenedClosedCard extends BaseObservable implements RecycleMultiLayoutInterface {
    private String opened;
    private String closed;

    public OpenedClosedCard(String opened, String closed) {
        this.opened = opened;
        this.closed = closed;
    }

    @Override
    public int getVariable() {
        return BR.card;
    }

    @Override
    public int getLayout() {
        return R.layout.card_opened_closed;
    }

    @Bindable
    public String getOpened() {
        return opened;
    }

    public void setOpened(String opened) {
        this.opened = opened;
        notifyPropertyChanged(BR.opened);
    }

    @Bindable
    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
        notifyPropertyChanged(BR.closed);
    }
}
