package br.com.concrete.desafio.model.card;

/**
 * Interface of events control of {@link RepositoryCard}
 */
public interface RepositoryCardInterface {
    /**
     * Event called when card is clicked.
     * @param card Card clicked.
     */
    void onRepositoryClick(RepositoryCard card);
}
