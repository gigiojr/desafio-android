package br.com.concrete.desafio.model.card;

import android.view.View;

import br.com.concrete.desafio.BR;
import br.com.concrete.desafio.R;
import br.com.concrete.desafio.util.RecycleMultiLayoutInterface;

/**
 * Layout Model Class to show params about Repository.
 *
 * xml: res/layout/card_repository
 * data: card
 *
 * @see TitleTextCard
 * @see ProfileVerticalCard
 * @see IconTextCard
 * @see RepositoryCardInterface
 * @see RecycleMultiLayoutInterface
 * @see br.com.concrete.desafio.util.RecycleMultiLayout
 */
public class RepositoryCard implements RecycleMultiLayoutInterface {
    private TitleTextCard title;
    private ProfileVerticalCard profile;
    private IconTextCard fork;
    private IconTextCard star;

    private RepositoryCardInterface events;

    public RepositoryCard(TitleTextCard title, ProfileVerticalCard profile, IconTextCard fork, IconTextCard star) {
        this.title = title;
        this.profile = profile;
        this.fork = fork;
        this.star = star;
    }

    /**
     * Event called when component is clicked.
     * @param view Component clicked.
     */
    public void onClick(View view){
        if(this.events != null){
            this.events.onRepositoryClick(this);
        }
    }

    @Override
    public int getVariable() {
        return BR.card;
    }

    @Override
    public int getLayout() {
        return R.layout.card_repository;
    }

    public TitleTextCard getTitle() {
        return title;
    }

    public void setTitle(TitleTextCard title) {
        this.title = title;
    }

    public ProfileVerticalCard getProfile() {
        return profile;
    }

    public void setProfile(ProfileVerticalCard profile) {
        this.profile = profile;
    }

    public IconTextCard getFork() {
        return fork;
    }

    public void setFork(IconTextCard fork) {
        this.fork = fork;
    }

    public IconTextCard getStar() {
        return star;
    }

    public void setStar(IconTextCard star) {
        this.star = star;
    }

    public RepositoryCardInterface getEvents() {
        return events;
    }

    public void setEvents(RepositoryCardInterface events) {
        this.events = events;
    }
}