package br.com.concrete.desafio.util;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;

import me.tatarka.bindingcollectionadapter2.ItemBinding;
import me.tatarka.bindingcollectionadapter2.OnItemBind;

/**
 * Class to create list of binding elements.
 *
 * Used to default items of {@link android.support.v7.widget.RecyclerView RecycleView}.
 */
public class RecycleMultiLayout {

    /** List of items **/
    public ObservableList<RecycleMultiLayoutInterface> list;

    /** List of bindings of list of items **/
    private OnItemBind<RecycleMultiLayoutInterface> itemBinding;

    public RecycleMultiLayout() {
        this.list = new ObservableArrayList<>();
        this.itemBinding = new OnItemBind<RecycleMultiLayoutInterface>() {
            @Override
            public void onItemBind(ItemBinding itemBinding, int position, RecycleMultiLayoutInterface item) {
                itemBinding.set(item.getVariable(), item.getLayout());
            }
        };
    }

    public OnItemBind<RecycleMultiLayoutInterface> getItemBinding() {
        return itemBinding;
    }

    public ObservableList<RecycleMultiLayoutInterface> getList() {
        return list;
    }

    public void setList(ObservableList<RecycleMultiLayoutInterface> list) {
        this.list = list;
    }

    public void setItemBinding(OnItemBind<RecycleMultiLayoutInterface> itemBinding) {
        this.itemBinding = itemBinding;
    }
}