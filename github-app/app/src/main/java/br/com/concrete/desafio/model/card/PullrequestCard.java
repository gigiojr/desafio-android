package br.com.concrete.desafio.model.card;

import android.view.View;

import java.util.Date;

import br.com.concrete.desafio.BR;
import br.com.concrete.desafio.R;
import br.com.concrete.desafio.util.DataBinding;
import br.com.concrete.desafio.util.RecycleMultiLayoutInterface;

/**
 * Layout Model Class to show params about pull request.
 *
 * xml: res/layout/card_repository_pullrequest
 * data: card
 *
 * @see TitleTextCard
 * @see ProfileHorizontalCard
 * @see PullRequestCardInterface
 * @see RecycleMultiLayoutInterface
 * @see br.com.concrete.desafio.util.RecycleMultiLayout
 */
public class PullrequestCard extends DataBinding implements RecycleMultiLayoutInterface {
    private TitleTextCard title;
    private Date date;
    private String url;
    private ProfileHorizontalCard profile;

    private PullRequestCardInterface events;

    public PullrequestCard(TitleTextCard title, Date date, String url, ProfileHorizontalCard profile) {
        this.title = title;
        this.date = date;
        this.url = url;
        this.profile = profile;
    }

    public void onClick(View view){
        if(this.events != null){
            this.events.onPullRequestClick(this);
        }
    }

    @Override
    public int getVariable() {
        return BR.card;
    }

    @Override
    public int getLayout() {
        return R.layout.card_repository_pullrequest;
    }

    public TitleTextCard getTitle() {
        return title;
    }

    public void setTitle(TitleTextCard title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ProfileHorizontalCard getProfile() {
        return profile;
    }

    public void setProfile(ProfileHorizontalCard profile) {
        this.profile = profile;
    }

    public PullRequestCardInterface getEvents() {
        return events;
    }

    public void setEvents(PullRequestCardInterface events) {
        this.events = events;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
