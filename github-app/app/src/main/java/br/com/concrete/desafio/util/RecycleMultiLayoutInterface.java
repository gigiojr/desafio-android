package br.com.concrete.desafio.util;

/**
 * Interface used to create an {@link android.support.v7.widget.RecyclerView RecycleView} with multiples layouts.
 *
 * The models should implement this and add to Array list used.
 */
public interface RecycleMultiLayoutInterface {
    /**
     * Get address of variable used in layout. Use {@link br.com.concrete.desafio.BR} class.
     * @return Variable address.
     */
    int getVariable();

    /**
     * Get address of layout. Use {@link br.com.concrete.desafio.R} class.
     * @return Layout address.
     */
    int getLayout();
}