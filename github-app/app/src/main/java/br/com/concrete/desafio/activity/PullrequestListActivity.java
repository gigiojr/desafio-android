package br.com.concrete.desafio.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import br.com.concrete.desafio.R;
import br.com.concrete.desafio.databinding.ActivityPullrequestListBinding;
import br.com.concrete.desafio.model.card.OpenedClosedCard;
import br.com.concrete.desafio.model.card.PullRequestCardInterface;
import br.com.concrete.desafio.model.card.PullrequestCard;
import br.com.concrete.desafio.util.RecycleMultiLayout;
import br.com.concrete.desafio.model.card.ProfileHorizontalCard;
import br.com.concrete.desafio.model.card.TitleTextCard;
import br.com.githubapi.Github;
import br.com.githubapi.model.PullRequest;
import br.com.githubapi.model.PullRequestCallback;
import br.com.githubapi.model.User;
import br.com.githubapi.model.UserCallback;

/**
 * Activity to list of repositories
 * XML: res/layout/activity_pullrequest_list
 *
 * @see RecycleMultiLayout
 * @see Github
 */
public class PullrequestListActivity extends AppCompatActivity implements PullRequestCardInterface {

    private Github githubAPI;
    private RecycleMultiLayout recycle;

    private OpenedClosedCard resumeCard;

    private int amountInsert;
    private int amountTotal;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pullrequest_list);

        ActivityPullrequestListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_pullrequest_list);

        this.recycle = new RecycleMultiLayout();
        binding.setRecycle(this.recycle);

        this.resumeCard = new OpenedClosedCard("0 opened", "0 closed");
        this.recycle.list.add(this.resumeCard);

        RecyclerView recycler = findViewById(R.id.recycle);
        recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        Intent intent = getIntent();
        String user = intent.getStringExtra("user");
        String repository = intent.getStringExtra("repository");
        if(user != null && repository != null){
            this.githubAPI = new Github();
            this.getPullRequests(user, repository);
        } else{
            finish();
        }
    }

    @Override
    public void onPullRequestClick(PullrequestCard card) {
        String url = card.getUrl();
        if(url != null && !url.isEmpty()) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    /**
     * Call Github API to get list of pull requests of the selected repository.
     * @param user Username of owner of repository.
     * @param repository Name of repository.
     */
    private void getPullRequests(String user, String repository){
        this.dialog = ProgressDialog.show(this, "", "Buscando...", true, false);
        this.githubAPI.getPullRequests(this, user, repository, new PullRequestCallback() {
            @Override
            public void onSuccess(List<PullRequest> list) {
                int opened = 0;
                int closed = 0;

                amountInsert = 0;
                amountTotal = list.size();

                for (PullRequest pullRequest : list){
                    getUser(pullRequest);

                    if(pullRequest.getState().equals("open")){
                        opened += 1;
                    } else if(pullRequest.getState().equals("closed")){
                        closed += 1;
                    }
                }
                resumeCard.setOpened(opened + " opened");
                resumeCard.setClosed(closed + " closed");
            }

            @Override
            public void onError() {
                dialog.dismiss();

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PullrequestListActivity.this);
                alertDialog.setTitle("Erro");
                alertDialog.setMessage("Não foi possível buscar os registros.");
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
    }

    /**
     * Call Github API to get params about user owner of the repository.
     * @param pullRequest Pull request object.
     */
    private void getUser(final PullRequest pullRequest){
        githubAPI.getUser(this, pullRequest.user.getUrl(), new UserCallback() {
            @Override
            public void onSuccess(User user) {
                pullRequest.setUser(user);
                includeCard(pullRequest);
            }

            @Override
            public void onError() {
                includeCard(pullRequest);
            }
        });
    }

    /**
     * Include pull request on recycler list.
     * @param pullRequest
     */
    private void includeCard(PullRequest pullRequest){
        TitleTextCard title = new TitleTextCard(pullRequest.getTitle(), pullRequest.getBody());
        ProfileHorizontalCard profile = new ProfileHorizontalCard(pullRequest.user.getLogin(), pullRequest.user.getName(), pullRequest.user.getAvatarUrl());

        PullrequestCard card = new PullrequestCard(title, pullRequest.getCreated(), pullRequest.getUrl(), profile);
        card.setEvents(this);
        this.recycle.list.add(card);

        this.amountInsert += 1;
        if(this.amountInsert == this.amountTotal){
            this.dialog.dismiss();
        }
    }
}