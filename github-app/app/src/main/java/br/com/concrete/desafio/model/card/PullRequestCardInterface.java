package br.com.concrete.desafio.model.card;

/**
 * Interface of events control of {@link PullrequestCard}
 */
public interface PullRequestCardInterface {
    /**
     * Event called when card is clicked.
     * @param card Card clicked.
     */
    void onPullRequestClick(PullrequestCard card);
}
