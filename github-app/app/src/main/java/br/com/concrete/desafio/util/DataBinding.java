package br.com.concrete.desafio.util;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Date;

/**
 * Class of Data binding definitions.
 */
public class DataBinding {

    /**
     * Method to load image into ImageView using a URL address.
     * @param view ImageView component.
     * @param url URL address of the image
     * @param error Local drawable that will be shown if the image of URL is not available
     */
    @BindingAdapter({"imageUrl", "imageError"})
    public static void loadImage(ImageView view, String url, Drawable error) {
        Picasso.get().load(url).error(error).into(view);
    }

    /**
     * Method to convert an date to string.
     * @param view TextView when date will show
     * @param date Date that will be show
     */
    @BindingAdapter({"convertDate"})
    public static void dateToString(TextView view, Date date) {
        if(date != null) {
            String day = (String) DateFormat.format("dd", date); // 20
            String month = (String) DateFormat.format("MM", date); // 06
            String year = (String) DateFormat.format("yyyy", date); // 2013
            String text = day + "/" + month + "/" + year;
            view.setText(text);
        }
    }
}
