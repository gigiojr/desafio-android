package br.com.concrete.desafio.model.card;

import br.com.concrete.desafio.util.DataBinding;

/**
 * Super class with basic parameters to layouts that show profile information.
 *
 * @see DataBinding#loadImage
 */
public class ProfileCard extends DataBinding{
    protected String login;
    protected String name;
    protected String imageUrl;

    public ProfileCard(String login, String name, String imageUrl) {
        this.login = login;
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImage(String image) {
        this.imageUrl = image;
    }
}
